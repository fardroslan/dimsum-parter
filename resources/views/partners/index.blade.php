<!DOCTYPE html>
<html lang="en">
<head>
	<title>Watch the Best of Asia on dimsum with digi</title>
	<base href="../../" />

	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="robots" content="index, follow" />

	<meta property="og:site_name" value="dimsum" />
	<meta property="og:title" content="dimsum - Serving you the best Asian content" />
	<meta property="og:url" content="https://partners.dimsum.my/en/digi/" />
	<meta property="og:image" content="https://edm.dimsum.my/og-image.jpg" />
	<meta property="og:description" content="Get your unlimited access to watch Asian entertainment on dimsum, for only 50 cent a day." />
	<meta name="description" content="Get your unlimited access to watch Asian entertainment on dimsum, for only 50 cent a day." />

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
  <link rel="stylesheet" href="{{getenv('BASE_URL_CSS')}}css/common.css" type="text/css" charset="utf-8" />
  <link rel="icon" href="https://edm.dimsum.my/favicon.ico" type="image/x-icon" />

	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-WX7TNM7');</script>
</head>

<body>
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WX7TNM7" class="gtm"></iframe></noscript><!-- Google Tag Manager (noscript) -->

		<div id="header-container" class="relative" style="background-image:url({{getenv('BASE_URL_IMAGES').'img/header/'.$partner['web_front_header_img'][$language]}})">
    <div class="absolute">
      <div class="inner-wrapper">
			<div class="logo-container clearfix">
					<img class="logo" src="{{getenv('BASE_URL_IMAGES').'img/logo/'.$common['web_front_logo'][$language]}}" /><img class="logo" src="{{getenv('BASE_URL_IMAGES').'img/logo/'.$partner['web_front_partners_logo'][$language]}}" />
			</div>
	        <h1 class="partner-title">{{$partner['web_front_inner-wrapper-h1'][$language]}}<br>{{$partner['web_front-inner-wrapper-h1-cont'][$language]}}</h1>
	        <h2>{{$partner['web_front_inner-wrapper-h2'][$language]}}</h2>
        	<a href="https://www.dimsum.my/signup" target="_blank" class="btn redeem-btn howtoredeem-btn text-2 font-bold">{{$common['web_front_redeem-btn-text2'][$language]}}</a>
      </div>
    </div>
		<div class="lang">
			<a href="zh/digi/">中文<a><span class="sep">|</span><a href="en/digi/">English</a>
		</div>
	</div>

	<div id="feature-container" class="text-align-center">
		<div class="container">
				<h3 class="text-1">{{$common['web_front_featured-container-text1'][$language]}}</h3>
				<h4 class="text-2 p-bottom-20">{{$common['web_front_featured-container-text2'][$language]}}</h4>
			<div class="icons-row clear-fix">
				<div class="col-md-6 kill-padding">
					<div class="col-xs-4">
							<img class="icons" src="{{getenv('BASE_URL_IMAGES').'img/icons/'.$common['web_front_featured-container-icon_img1'][$language]}}">
							<p class="icon-text-1">{{$common['web_front_featured-container-icon-headingtext1'][$language]}}</p>{{$common['web_front_featured-container-icon-text1'][$language]}}
					</div>
					<div class="col-xs-4">
						<img class="icons" src="{{getenv('BASE_URL_IMAGES').'img/icons/'.$common['web_front_featured-container-icon_img2'][$language]}}">
						<p class="icon-text-1">{{$common['web_front_featured-container-icon-headingtext2'][$language]}}</p>{{$common['web_front_featured-container-icon-text2'][$language]}}
					</div>
					<div class="col-xs-4">
						<img class="icons" src="{{getenv('BASE_URL_IMAGES').'img/icons/'.$common['web_front_featured-container-icon_img3'][$language]}}">
						<p class="icon-text-1">{{$common['web_front_featured-container-icon-headingtext3'][$language]}}</p>{{$common['web_front_featured-container-icon-text3'][$language]}}
					</div>
				</div>
				<div class="col-md-6 kill-padding">
					<div class="col-xs-4">
						<img class="icons" src="{{getenv('BASE_URL_IMAGES').'img/icons/'.$common['web_front_featured-container-icon_img4'][$language]}}">
						<p class="icon-text-1">{{$common['web_front_featured-container-icon-headingtext4'][$language]}}</p>{{$common['web_front_featured-container-icon-text4'][$language]}}
					</div>
					<div class="col-xs-4">
						<img class="icons" src="{{getenv('BASE_URL_IMAGES').'img/icons/'.$common['web_front_featured-container-icon_img5'][$language]}}">
						<p class="icon-text-1">{{$common['web_front_featured-container-icon-headingtext5'][$language]}}</p>{{$common['web_front_featured-container-icon-text5'][$language]}}
					</div>
					<div class="col-xs-4">
						<img class="icons" src="{{getenv('BASE_URL_IMAGES').'img/icons/'.$common['web_front_featured-container-icon_img6'][$language]}}">
						<p class="icon-text-1">{{$common['web_front_featured-container-icon-headingtext6'][$language]}}</p>{{$common['web_front_featured-container-icon-text6'][$language]}}
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="step-container" class="text-align-center">
		<div class="container">

      <h3 class="text-1">{{$common['web_front_step-container-text1'][$language]}}</h3>
			<h4 class="text-2">{{$common['web_front_step-container-text2'][$language]}}</h4>
			<div class="redeem-row clearfix">
				<div class="col-xs-12 col-sm-4 col-md-4">
					<img src="{{getenv('BASE_URL_IMAGES').'img/step/'.$common['web_front_step-container-redeem_img'][$language]}}" />{{$common['web_front_step-container-redeem_text'][$language]}}
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4">
					<img src="{{getenv('BASE_URL_IMAGES').'img/step/'.$common['web_front_step-container-promo_img'][$language]}}" />{{$common['web_front_step-container-promo_text'][$language]}}
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4">
					<img src="{{getenv('BASE_URL_IMAGES').'img/step/'.$common['web_front_step-container-watch_img'][$language]}}" />{{$common['web_front_step-container-watch_text'][$language]}}
				</div>
			</div>
			<a href="https://www.dimsum.my/signup" target="_blank" class="btn redeem-btn text-2 font-bold">{{$common['web_front_redeem-btn-text2'][$language]}}</a>
			<p class="redeem-text">
				<b>{{$common['web_front_step-container-for_web_header1'][$language]}}</b><br> {{$common['web_front_step-container-for_web_header1_descs'][$language]}}<br/>
				<b>{{$common['web_front_step-container-for_web_header2'][$language]}}</b><br> {{$common['web_front_step-container-for_web_header2_descs'][$language]}}<br/>
				<b>{{$common['web_front_step-container-for_web_header3'][$language]}}</b><br> {{$common['web_front_step-container-for_web_header3_descs'][$language]}}
			</p>
		</div>
	</div>

	<div id="faq-container">
		<div class="container">
			<h3 class="text-1 text-black text-align-center">{{$common['web_front_faq-container_text-1'][$language]}}</h3>

      <div id="accordion" class="panel-group" role="tablist" aria-multiselectable="true">
				<?php $i=0; ?>
				@foreach($partner['web_front_faq-container_heading'] as $item )
					<?php $i++; ?>
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="heading{{$i}}">
            <h4 class="panel-title">
              <a role="button" data-toggle="collapse" href="#collapse{{$i}}" aria-expanded="true" aria-controls="collapse{{$i}}">
                <i class="more-less glyphicon glyphicon-minus"></i>
                {{$i.'.'.$item[$language]}}
              </a>
            </h4>
          </div>

          <div id="collapse{{$i}}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading{{$i}}">
            <div class="panel-body">
							<p style="white-space:pre-line;">{{$item[$language.'-body']}}</p>
            </div>
          </div>
        </div>
				@endforeach
      </div>
		</div>
	</div>

	<div id="contact-us" class="text-align-center">
		<div class="container">
			<h3 class="text-1">{{$common['web_front_contact-us-container_text-1'][$language]}}</h3>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="contact-us-box">
						<img class="mail-icon pull-left" src="{{getenv('BASE_URL_IMAGES').'img/contact/'.$common['web_front_contact-us-container_img'][$language]}}">
						<p class="pull-left mail-icon-text">
							{{$common['web_front_contact-us-container_contact_us_box-text'][$language]}}<br>
						</p>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="contact-us-box">
						<img class="phone-icon pull-left" src="{{getenv('BASE_URL_IMAGES').'img/contact/'.$common['web_front_contact-us-container_contact_us_box_phone-icon'][$language]}}">
						<p class="pull-left" style="white-space: pre;">{{$common['web_front_contact-us-container_contact_us_box_long-desc'][$language]}}</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="terms">
		<div class="container text-align-center">
			<div class="row">
				<h2 class="text-black font-bold">{{$common['web_front_terms-container_h2'][$language]}}</h2>
				<p class="text-black font-bold text-3">{{$common['web_front_terms-container_text-3'][$language]}}</p>
				<p class="text-black font-bold"><a href="https://www.dimsum.my/terms" target="_blank" class="terms-btn">* <u>{{$common['web_front_terms-container_link-text'][$language]}}</u></a></p>
			</div>
			<div class="row">
				<a href="https://www.dimsum.my/signup" target="_blank" class="btn redeem-btn text-2 font-bold">{{$common['web_front_redeem-btn-text2'][$language]}}</a>
				<p class="redeem-text text-black">
					<b>{{$common['web_front_step-container-for_web_header1'][$language]}}</b><br> {{$common['web_front_step-container-for_web_header1_descs'][$language]}}<br/>
					<b>{{$common['web_front_step-container-for_web_header2'][$language]}}</b><br> {{$common['web_front_step-container-for_web_header2_descs'][$language]}}<br/>
					<b>{{$common['web_front_step-container-for_web_header3'][$language]}}</b><br> {{$common['web_front_step-container-for_web_header3_descs'][$language]}}
				</p>
			</div>
		</div>
	</div>

	<div id="footer" class="clearfix">
		<a class="footer-icon" href="{{$common['web_front_footer_footer-icon'][$language]}}" target="_blank"><img src="{{$common['web_front_footer_footer-img'][$language]}}" /></a>
    <a class="footer-icon" href="{{$common['web_front_footer_footer-icon2'][$language]}}" target="_blank"><img src="{{$common['web_front_footer_footer-img2'][$language]}}" /></a>
    <div class="footer-sitemap">
			<a href="{{$common['web_front_footer_footer-sitemap_text_link'][$language]}}" target="_blank">{{$common['web_front_footer_footer-sitemap_text'][$language]}}</a>
			<a href="{{$common['web_front_footer_footer-sitemap_text_link2'][$language]}}" target="_blank">{{$common['web_front_footer_footer-sitemap_text2'][$language]}}</a>
			<a href="{{$common['web_front_footer_footer-sitemap_text_link3'][$language]}}" target="_blank">{{$common['web_front_footer_footer-sitemap_text3'][$language]}}</a>
			<a href="{{$common['web_front_footer_footer-sitemap_text_link4'][$language]}}" target="_blank">{{$common['web_front_footer_footer-sitemap_text4'][$language]}}</a>
			<a href="{{$common['web_front_footer_footer-sitemap_text_link5'][$language]}}" target="_blank" class="no-border">{{$common['web_front_footer_footer-sitemap_text5'][$language]}}</a>
      <div class="small">&copy; <?php echo date("Y");?> {{$common['web_front_footer_footer-sitemap_text6'][$language]}}</div>
    </div>
    <div class="footer">
			<li class="small">{{$common['web_front_footer_footer-small-word'][$language]}}</li><img class="smg-logo" src="{{$common['web_front_footer_footer-small-word_logo'][$language]}}" />
    </div>
	</div>
	
	<script src="{{getenv('BASE_URL_JS')}}js/master.js"></script>
</body>
</html>
