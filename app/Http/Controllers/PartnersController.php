<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;

class PartnersController extends BaseController
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */

     //public $base_url_partner = '../../../../public/js/digi.json';
     //public $base_url_common = '../../../../public/js/partner.json';

    public function partners_en($partners)
    {
      //$request_url = $request->path();
      //dd(getenv('BASE_URL_PARTNER'));
      //$partners_path = realpath(__DIR__.'../../../../public/js/digi.json');

      $partners_path = getenv('BASE_URL_PARTNER') . "json/$partners.json";

      $partner = file_get_contents($partners_path);

      //$common_path = realpath(__DIR__.'../../../../public/js/partner.json');
      $common_path = getenv('BASE_URL_COMMON') . "json/partner.json";

      $common = file_get_contents($common_path);

      $json_file = pathinfo($partners_path, PATHINFO_FILENAME);

      if($partners == $json_file){
        return view('partners.index')->with('common', json_decode($common, true))
                                    ->with('partner', json_decode($partner,true))
                                    ->with('language','en');
      }else{
        return View::make('errors.404');
      }

    }

    public function partners_zh($partners)
    {
      //$request_url = $request->path();
      //$partners_path = realpath(__DIR__.'../../../../public/js/digi.json');
      $partners_path = getenv('BASE_URL_PARTNER') . "json/$partners.json";
      $partner = file_get_contents($partners_path);


      //$common_path = realpath(__DIR__.'../../../../public/js/partner.json');
      $common_path = getenv('BASE_URL_COMMON') . "json/partner.json";
      $common = file_get_contents($common_path);
      $json_file = pathinfo($partners_path, PATHINFO_FILENAME);

      if($partners == $json_file){
        return view('partners.index')->with('common', json_decode($common, true))
                                    ->with('partner', json_decode($partner,true))
                                    ->with('language','zh');
      }else{
        return "404";
      }

    }

}
