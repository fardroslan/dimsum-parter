<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->welcome();
});



/*$app->get('user/{id}', function ($id) use ($app) {
    return 'User '.$id;
});*/

//Jacky : Go to the Digi English version
$app->get('/en/{partners}/','PartnersController@partners_en');

//$app->get('/ms/{partners}/','PartnersController@partners');

$app->get('/zh/{partners}/','PartnersController@partners_zh');


//Jacky : Go to the Digi English version
//$app->get('/{language}/digi/','DigiEnController@englishver');

//Jacky : Go to the Digi Chinese version
//$app->get('/zh/digi','DigiEnController@zhver');
