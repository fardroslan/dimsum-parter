[
  'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js',
  'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js',
  'js/local.js',
  'https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js',
  'https://oss.maxcdn.com/respond/1.4.2/respond.min.js'
].forEach(function(src) {
  var script = document.createElement('script');
  script.src = src;
  script.async = false;
  document.head.appendChild(script);
});